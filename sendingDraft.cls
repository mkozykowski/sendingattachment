List<Id> attachmentIdsList = new List<Id>();
for (Attachment att : [SELECT ParentId FROM Attachment where Parent.Type = 'DeliveryNote__c' and Name like '%-SG.pdf']){
	attachmentIdsList.add(att.ParentId);
}
List<DeliveryNote__c> resultDeliveryNote = [SELECT Id FROM DeliveryNote__c WHERE ID NOT IN :attachmentIdsList];   
System.debug('resultDeliveryNote: '+resultDeliveryNote);

string header = 'Delivery Note without PDF Id \n';
string finalstr = header ;

for(DeliveryNote__c dn :resultDeliveryNote){
		string recordString = dn.Id +'\n';
        finalstr = finalstr +recordString;
}


Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
blob csvBlob = Blob.valueOf(finalstr);
string csvname= 'dn_without_pdf.csv';
csvAttc.setFileName(csvname);
csvAttc.setBody(csvBlob);
Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
String[] toAddresses = new list<string> {'xxx@xx.com'};
String subject ='dn without pdf CSV';
email.setSubject(subject);
email.setToAddresses( toAddresses );
email.setPlainTextBody('In the attachment is csv file with all delivery notes without ');
email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});